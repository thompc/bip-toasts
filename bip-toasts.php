<?php
/*
Plugin Name: BiP Toasts
Description: Adds toast functionality
Version: 1.1
Author: BiP Web Developers
Text Domain: bip-toasts
GitHub Plugin URI: https://bitbucket.org/thompc/bip-toasts
*/


function register_the_session(){
    //session_destroy();
    if( !session_id() ){
    session_start();
    }

}

add_action('init', 'register_the_session');


    /**
     * Define ajax for the frontend
     *
     * @return void
     */
    function define_ajax_frontend(){
        echo '<script type="text/javascript">
            var toastajaxurl = "' . admin_url('admin-ajax.php') . '";
        </script>';
    }

    add_action('wp_head', 'define_ajax_frontend');



function bip_toast_styles_and_scripts(){
    $dir = plugin_dir_path( __FILE__ );
    wp_enqueue_script(
        'bip-toast-scripts',
        "//cdn.rawgit.com/kamranahmedse/jquery-toast-plugin/9ed0b7ac/src/jquery.toast.js",
        array('jquery'),
        null
    );

    wp_register_style(
        'bip-toast-styles',
        "//cdn.rawgit.com/kamranahmedse/jquery-toast-plugin/9ed0b7ac/src/jquery.toast.css",
        false,
        null
        );
        wp_enqueue_style('bip-toast-styles');

}

add_action( 'wp_enqueue_scripts', 'bip_toast_styles_and_scripts' );



/**
* bip_add_tracking_scripts
*
* @type     action
* @date     22/12/2017
*
* Place scripts from defined in BiP Settings into wordpress header on specific pages. 
*
* @param    n/a
*
* @return   n/a
*
*/
function bip_add_toast_scripts(){

    global $post;
    global $wp_current_filter;

    

    // Get toasts
    $toasts = get_field('bip_blank_toasts', 'options');

    if($toasts){

        foreach ($toasts as $toast) {
            
            $expiry = $toast['toast_expiry_date'];

            if( strtotime($expiry) < strtotime('now') ) {
                $expired = true;
            }else{
                $expired = false;
            }

            if(!$expired){
            
                // If for all pages then display
                if($toast['bip_blank_toast_pages_or_selected'] === 'all'){


                    if(!isset($_SESSION['bip_seen_toast']) &&  $_SESSION['bip_seen_toast'] != $toast['toast_unique_id']) {
                        // Do javascript
                        echo bip_do_toast_javascript($toast);

                    }


                }

                // If for specific pages
                if($toast['bip_blank_toast_pages_or_selected'] === 'selected'){

                    if(!empty($script['bip_select_pages_toast'])){

                        foreach ($script['bip_select_pages_toast'] as $page_id) {
                        
                            if(intval($page_id) === intval($post->ID)){ 

                                if(!isset($_SESSION['bip_seen_toast']) &&  $_SESSION['bip_seen_toast'] != $toast['toast_unique_id']) {
                                    // Do javascript
                                    echo bip_do_toast_javascript($toast);

                                }
                                   

                            }

                        }

                    }

                }

            }

            
        }
        
    }

}

// The three possible locations where scripts can be added
add_action('wp_head', 'bip_add_toast_scripts');


function bip_do_toast_javascript($toast){

    ob_start() ?>

        <style>
            .jq-toast-single{
                max-width: 100%;
                font-size: 18px;
                padding: 1em;
                position:relative;
                box-sizing: border-box;
            }
            .close-jq-toast-single{
                color: <?php echo $toast['toast_text_color']; ?>;
            }
            .jq-toast-single p{
                padding: 0.5em 0;
            }
            .jq-toast-wrap{ 
                max-width: calc(100% - 35px);
                max-height: calc(100% - 35px);
                overflow-y: auto;
                width: 350px;
            }

            .jq-toast-single p {
                color: inherit;
                font-size: 0.8em;
                font-weight: 200;
            }
            .jq-toast-single h3 {
                color: inherit;
                font-size: 1em;
                font-weight: bold;
            }
        </style>

        <script>
        
            jQuery(document).ready(function(){

                window.setTimeout(function(){
                    var thetoast = jQuery.toast({
                        text : "<?php echo $toast['bip_blank_toast_content']; ?>", 
                        showHideTransition : '<?php echo $toast['toast_transition']; ?>',
                        bgColor : '<?php echo $toast['toast_background_colour']; ?>', 
                        textColor : '<?php echo $toast['toast_text_color']; ?>',
                        allowToastClose : <?php echo $toast['toast_show_close_button']; ?>,  
                        hideAfter : <?php echo $toast['toast_hide_after']; ?> * 1000, 
                        textAlign : '<?php echo $toast['toast_text_align']; ?>', 
                        position : '<?php echo $toast['toast_position']; ?>',
                        beforeShow: function(e){
                            e.attr('id', '<?php echo $toast['toast_unique_id'];?>');
                            e.attr('data-id', '<?php echo $toast['toast_unique_id'];?>');
                            jQuery("#<?php echo $toast['toast_unique_id'];?> .close-jq-toast-single").click(function(){
                                
                                var data = {
                                    action : 'set_toast_session',
                                    toast : '<?php echo $toast['toast_unique_id'];?>'
                                }
                                jQuery.post(toastajaxurl, data, function(response){});

                            });
                        }
                       
                });

                }, 1000);
  
            });
        
        
        </script>


     <?php 

        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

}


function set_toast_session(){

    $_SESSION['bip_seen_toast'] = $_POST['toast'];

    wp_die();

}

add_action('wp_ajax_set_toast_session','set_toast_session');
add_action('wp_ajax_nopriv_set_toast_session', 'set_toast_session');


/** 
*
* Create an field set in BiP Settings to allow control toast being added to
* the header 
*
*/
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5a3cd179a7e15aa',
        'title' => 'Toast',
        'fields' => array(
            array(
                'key' => 'field_5a3cd18638676aa',
                'label' => 'Toasts',
                'name' => 'bip_blank_toasts',
                'type' => 'repeater',
                'instructions' => 'Add notifications to the site',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'block',
                'button_label' => 'Add Toast',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5a3cd1f838677aa',
                        'label' => 'Add toast to all pages or selected?',
                        'name' => 'bip_blank_toast_pages_or_selected',
                        'type' => 'radio',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'all' => 'All',
                            'selected' => 'Selected',
                        ),
                        'allow_null' => 0,
                        'other_choice' => 0,
                        'save_other_choice' => 0,
                        'default_value' => 'all',
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                    ),
                    array(
                        'key' => 'field_5a3cd3402b6c0aa',
                        'label' => 'Select Page(s) to add the toast',
                        'name' => 'bip_select_pages_toast',
                        'type' => 'post_object',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5a3cd1f838677aa',
                                    'operator' => '==',
                                    'value' => 'selected',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'post_type' => array(
                            0 => 'page',
                            1 => 'post',
                        ),
                        'taxonomy' => array(
                        ),
                        'allow_null' => 0,
                        'multiple' => 1,
                        'return_format' => 'id',
                        'ui' => 1,
                    ),
                    
                    array(
                        'key' => 'field_5a3cd2b538678aa',
                        'label' => 'Content',
                        'name' => 'bip_blank_toast_content',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => '',
                        'new_lines' => '',
                    ),
                    array(
                        'key' => 'field_5b61affd7b929',
                        'label' => 'Transition',
                        'name' => 'toast_transition',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'plain' => 'Plain',
                            'fade' => 'Fade',
                            'slide' => 'Slide',
                        ),
                        'default_value' => array(
                            0 => 'slide',
                        ),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_5b61b0607b92a',
                        'label' => 'Background Colour',
                        'name' => 'toast_background_colour',
                        'type' => 'color_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#35A935',
                    ),
                    array(
                        'key' => 'field_5b61b0c77b92b',
                        'label' => 'Text Color',
                        'name' => 'toast_text_color',
                        'type' => 'color_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#FFFFFF',
                    ),
                    array(
                        'key' => 'field_5b61b0f17b92c',
                        'label' => 'Show Close Button?',
                        'name' => 'toast_show_close_button',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'default_value' => 1,
                        'ui' => 1,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'field_5b61b11d7b92d',
                        'label' => 'Hide After',
                        'name' => 'toast_hide_after',
                        'type' => 'text',
                        'instructions' => 'Set to 0 to remain on page',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 40,
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => 'seconds',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5b6709627e01aaa',
                        'label' => 'expiry date',
                        'name' => 'toast_expiry_date',
                        'type' => 'date_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'default_value' => '11/8/2018',
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'display_format' => 'd/m/Y',
                        'return_format' => 'd/m/Y',
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'field_5b61b1627b92e',
                        'label' => 'Text Align',
                        'name' => 'toast_text_align',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'left' => 'Left',
                            'right' => 'Right',
                            'center' => 'Center',
                        ),
                        'default_value' => array(
                        ),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_5b61b1977b92f',
                        'label' => 'Position',
                        'name' => 'toast_position',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'bottom-left' => 'Bottom Left',
                            'bottom-right' => 'Bottom Right',
                            'bottom-center' => 'Bottom Center',
                            'top-left' => 'Top Left',
                            'top-right' => 'Top Right',
                            'top-center' => 'Top Center',
                            'mid-center' => 'Mid Center',
                        ),
                        'default_value' => array(
                            0 => 'bottom-left',
                        ),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_5b61b3ecdd098',
                        'label' => 'Once Closed don\'t open again for',
                        'name' => 'open_again_after_closed_for',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 7,
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => 'days',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5b61b6f7056dd',
                        'label' => 'Unique ID',
                        'name' => 'toast_unique_id',
                        'type' => 'text',
                        'readonly' => 1,
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-toast-settings',
                ),
            ),
        ),
        'menu_order' => 15,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
   add_filter('acf/load_field/name=toast_unique_id', 'add_unique_id');
endif;
function add_unique_id( $field ) {       
    $field['default_value'] = uniqid();
    return $field;
}


/**
 * 
 * Setup an options page
 */
if(function_exists('acf_add_options_page')){
    // add sub page
    acf_add_options_page(array(
        'page_title'    => 'Toasts',
        'menu_title'    => 'Toasts',
        'menu_slug'     => 'theme-toast-settings',
        'icon_url' => 'dashicons-welcome-comments',
    ));
}  
 ?>